# == Schema Information
#
# Table name: patients
#
#  id              :bigint           not null, primary key
#  email           :string
#  name            :string
#  password_digest :string
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#
require 'rails_helper'

RSpec.describe Patient, type: :model do
  describe 'Validations' do
    it { should validate_presence_of :name }
    it { should validate_uniqueness_of :name }
  end

  describe 'Associations' do
    it { should have_many(:appointments) }
    it { should have_many(:doctors) }
  end
end
