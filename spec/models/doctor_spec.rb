# == Schema Information
#
# Table name: doctors
#
#  id              :bigint           not null, primary key
#  address         :string
#  city            :string
#  email           :string
#  name            :string
#  password_digest :string
#  title           :string
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#
require 'rails_helper'

RSpec.describe Doctor, type: :model do
  describe 'Validations' do
    it { should validate_presence_of :name }
    it { should validate_uniqueness_of :name }
  end

  describe 'Associations' do
    it { should have_many(:appointments).dependent(:destroy) }
    it { should have_many(:patients) }
  end
end
