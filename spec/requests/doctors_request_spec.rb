require 'rails_helper'

RSpec.describe "Doctors", type: :request do

  describe "GET /new" do
    it "returns http success" do
      get "/doctors/new"
      expect(response).to have_http_status(:success)
      expect(response).to render_template(:new)
    end

    it "does render new template" do
      get "/doctors/new"
      expect(response).to_not render_template(:show)
    end
  end

  it "does not render a different template" do
    get "/doctors/new"
    expect(response).to_not render_template(:show)
  end
end
