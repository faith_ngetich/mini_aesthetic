# == Schema Information
#
# Table name: doctors
#
#  id              :bigint           not null, primary key
#  address         :string
#  city            :string
#  email           :string
#  name            :string
#  password_digest :string
#  title           :string
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#
FactoryBot.define do
  factory :doctor do
    name { Faker::Name.first_name }
    title { "Plastic Sergeon" }
    city { Faker::Internet.city }
    address { Faker::Internet.address }
  end
end
