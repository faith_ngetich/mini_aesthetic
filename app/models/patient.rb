# == Schema Information
#
# Table name: patients
#
#  id              :bigint           not null, primary key
#  email           :string
#  name            :string
#  password_digest :string
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#
class Patient < ApplicationRecord
  has_many :appointments
  has_many :doctors, through: :appointments
  validates :name, presence:true, uniqueness: true

  has_secure_password
end
