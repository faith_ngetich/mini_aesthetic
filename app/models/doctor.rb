# == Schema Information
#
# Table name: doctors
#
#  id              :bigint           not null, primary key
#  address         :string
#  city            :string
#  email           :string
#  name            :string
#  password_digest :string
#  title           :string
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#
class Doctor < ApplicationRecord
  has_many :appointments, dependent: :destroy
  has_many :patients, through: :appointments

  validates :name, presence: true, uniqueness: true

  has_secure_password
end
