# == Schema Information
#
# Table name: appointments
#
#  id         :bigint           not null, primary key
#  date       :string
#  day        :string
#  location   :string
#  reason     :string
#  time       :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  doctor_id  :bigint           not null
#  patient_id :bigint           not null
#
# Indexes
#
#  index_appointments_on_doctor_id   (doctor_id)
#  index_appointments_on_patient_id  (patient_id)
#
# Foreign Keys
#
#  fk_rails_...  (doctor_id => doctors.id)
#  fk_rails_...  (patient_id => patients.id)
#
class Appointment < ApplicationRecord
  belongs_to :doctor
  belongs_to :patient

  scope :today, -> {where(date: Date.today)}
  scope :this_week, -> {where("date >= ? AND date <= ?", Date.today, Date.today+1.week )}

end
