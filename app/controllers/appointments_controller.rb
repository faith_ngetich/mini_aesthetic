class AppointmentsController < ApplicationController
  before_action :appointment, only: %i[show destroy]

  include ActionView::Helpers::UrlHelper

  # GET /appointments
  def index
    # @appointments = Appointment.all
    if @current_patient
      @appointments = @current_patient.appointments
    elsif @current_doctor
      @appointments = @current_doctor.appointments
      respond_to do |format|
        format.html { render 'index.html' }
        format.json { render json: @appointments.today }
      end
    else
      flash[:alert] = "Log in first to view appointments"
      redirect_to root_path
    end
  end

  def show
    if @current_patient
      unless @current_patient == @appointment.patient
        flash[:alert] = "You are not allowed to view this appointment."
        redirect_to appointments_path
      end
    elsif @current_doctor
      unless @current_doctor == @appointment.doctor
        flash[:alert] = "You are not allowed to view this appointment."
        redirect_to appointments_path
      end
    else
      flash[:alert] = "You can't view appointments because you aren't logged in."
      redirect_to root_path
    end
  end

  def new
    @appointment = Appointment.new
  end

  def edit
    if @current_doctor
      unless @current_doctor == @appointment.doctor
        flash[:alert] = "You are not allowed to edit this appointment."
        redirect_to appointments_path
      end
    elsif @current_patient
      unless @curent_patient == @appointment.patient
        flash[:alert] = "You are not allowed to edit appointment."
        redirect_to appointments_path
      end
    end
  end

   # POST /appointments
  def create
    @appointment = Appointment.new(appointment_params)
    if @appointment.save
      flash[:notice] = 'Appointment sucessfully created'
      redirect_to appointment_path(@appointment)
    else
      flash.now[:alert] = 'Appointment creation not successful'
      render :new
    end
  end

  # PUT /appointments/1
  def update
    # @appointment = Appointment.update_attribute(params[:id])
    # @appointment.update_attributes(:user_id => current_user.id)
    # if @appointment.save
    #   flash[:notice] = 'Update confirmed!'
    #   redirect_to doctor_appointments_path
    # else
    #   render :new
    # end
  end

  def destroy
    if @current_doctor || @curent_patient
      if @current_doctor == @appointment.doctor || @current_patient == @ppointment.patient
        @appointment.destroy
        flash[:notice] = 'Your appointment has been cancelled successfully '
        redirect_to appointments_path
      else
        flash[:alert] = 'You are not allowed to cancel this appointment.'
        redirect_to appointments_path
      end
    end
  end

  private

  def appointment
    @appointment = Appointment.find(params[:id])
  end

  def appointment_params
    params.require(:appointment).permit(:patient_id, :date, :time, :doctor_id, :reason )
  end
end
