class ApplicationController < ActionController::Base
  helper_method :logged_in?, :current_doctor, :current_patient
  before_action :current_doctor, :current_patient

  def current_patient
    @current_patient ||= Patient.find(session[:patient_id]) if session[:patient_id].present?
  end

  def current_doctor
    @current_doctor ||= Doctor.find(session[:doctor_id]) if session[:doctor_id].present?
  end

  def logged_in?
    if !current_doctor.nil?
      !!current_doctor
    elsif !current_patient.nil?
      !!current_patient
    else
      false
    end
  end

  def login_doctor(doctor)
    session[:doctor_id] = doctor.id
  end

  def login_patient(patient)
    session[:patient_id] = patient.id
  end

  def redirect
    if @current_doctor
      redirect_to doctor_path(@current_doctor)
    elsif @current_patient
      redirect_to patient_path(@current_patient)
    end
  end

end
