class DoctorsController < ApplicationController
  # skip_before_action :authorized, only: [:new, :create]

  def index
    @doctors = Doctor.all
    respond_to do |format|
      format.html { render 'index.html' }
      format.json { render json: @doctors}
    end
  end

  def new
    if @current_doctor
      flash[:alert] = "You already have an account."
      redirect_to doctor_path(@current_doctor)
    elsif @current_patient
      flash[:alert] = "You are not allowed to create a Doctor account."
      redirect_to patient_path(@current_patient)
    end
    @doctor = Doctor.new
  end

  def show
    if @current_doctor
      @all_appointments = @current_doctor.appointments
      @appointments = Appointment.today.where("doctor_id = ?", current_doctor.id)
      @appointment = Appointment.new
      respond_to do |format|
        format.html { render 'show.html' }
        format.json { render json: @all_appointments}
      end
    else
      @appointments = nil
    end
  end

  def create
    @doctor = Doctor.new(doctor_params)

    if @current_doctor
      flash[:alert] = "You already have an account."
      redirect_to doctor_path(@current_doctor)
    elsif @current_patient
      flash[:alert] = "You can't create a Doctor account."
      redirect_to patient_path(patient)
    end

    if @doctor.save
      login_doctor(@doctor)
      flash[:notice] = "You have successfully signed up."
      redirect_to doctor_path(@doctor)
    else
      flash.now[:alert] = "Signup failed."
      render :new
    end
  end

  def destroy
    if @current_doctor
      if @current_doctor == @doctor
        @doctor.destroy
        reset_session
        flash[:notice] = "Acount successfully deleted."
        redirect_to root_path
      else
        flash[:alert] = "You are not allowed to delete another Doctors account."
      end
    else
      flash[:alert] = "You are not allowed to delete a Doctor."
    end
  end

  private

  def doctor_params
    params.require(:doctor).permit(:name, :email, :title, :city, :address, :password)
  end
end
