class PatientsController < ApplicationController
  # skip_before_action :authorized, only: [:new, :create]
  before_action :patient, only: %i[show destroy]

  def show
    if @current_patient = @patient
      @appointments = Appointment.this_week.where("patient_id = ?", @current_patient.id)
      respond_to do |format|
        format.html { render 'show.html' }
        format.json { render json: @patient}
      end
    else
      if @current_doctor
        redirect_to doctor_path(@current_doctor)
      elsif @current_patient
        redirect_to patient_path(@current_patient)
      else
        redirect_to root_path
      end
    end
  end

  def new
    redirect
    @patient = Patient.new
  end

  def create
    @patient = Patient.new(patient_params)
    if @current_patient
      flash[:alert] = "This account exits."
      redirect_to patient_path(@current_patient)
    elsif @current_doctor
      flash[:alert] = "You are not allowed to create a patient account."
      redirect_to patient_path(@patient)
    end
    if @patient.save
      login_patient(@patient)
      flash[:notice] = "Successfully signed up."
      redirect_to patient_path(@patient)
    else
      flash.now[:alert] = "Signup failed. Patient account couldn't be created."
      render :new
    end
  end

  def destroy
    if @current_patient
      if @current_patient == @patient
        @patient.destroy
        reset_session
        flash[:notice] = "Successfully deleted your account."
        redirect_to :root
      else
        flash[:alert] = "You are not allowed to delete another Patients account."
      end
    else
      flash[:alert] = "You are not allowed to delete a Patient."
    end
  end

  private
  def patient
    @patient = Patient.find(params[:id])
  end

  def patient_params
    params.require(:patient).permit(:name, :email, :password)
  end
end
