class SessionsController < ApplicationController

  def new
    if logged_in?
      redirect
    end
  end

  def create
    if params[:email].present? && params[:password].present?
      @doctor = Doctor.find_by(email: params[:email])
      @patient = Patient.find_by(email: params[:email])
      if @doctor && @doctor.authenticate(params[:session][:password])
        login_doctor(doctor)
        redirect_to '/welcome'
      elsif @patient && @patient.authenticate(params[:session][:password])
        login_patient(patient)
        redirect_to '/welcome'
      else
        flash.now[:alert] = 'Account not found. Please try again with correct credentials.'
        redirect_to '/login'
      end
    end
  end

  def welcome
  end

  def destroy
    reset_session
    flash[:notice] = "You have successfully logged out."
    redirect_to root_path
  end

end
