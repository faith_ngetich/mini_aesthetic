[![Ruby Style Guide](https://img.shields.io/badge/code_style-rubocop-brightgreen.svg)](https://github.com/rubocop-hq/rubocop)
[![Gem Version](https://badge.fury.io/rb/rubocop.svg)](https://badge.fury.io/rb/rubocop)

# Mini_aesthetic

A Ruby on Rails application that allows patients to create doctor appointments


## Example


 ## Installation.
 
 Clone the Repo in a folder of your choice
 
 - $`git clone https://gitlab.com/faith_ngetich/mini_aesthetic.git`
 
 Navigate to the root folder.
 
 - $`cd mini_aesthetic`
 
 Install gems required locally.
 
 - $`bundle`
 
 
 ##  How to run the application
   - $ `bundle exec rails server`
 

 
 ## How to run the test suite
 
 Run
 - $ `bundle exec rspec`
 

