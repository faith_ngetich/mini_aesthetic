class CreateDoctors < ActiveRecord::Migration[6.0]
  def change
    create_table :doctors do |t|
      t.string :name
      t.string :title
      t.string :city
      t.string :address

      t.timestamps
    end
  end
end
