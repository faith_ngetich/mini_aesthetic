Rails.application.routes.draw do
  resources :doctors do
    resources :appointments
  end

  resources :appointments

  resources :patients

  get 'login', to: 'sessions#new'
  post 'login', to: 'sessions#create'
  get 'welcome', to: 'sessions#welcome'
  delete 'logout', to: 'sessions#destroy'
  # get 'authorized', to: 'sessions#require_login'

  root 'home#about'
end
